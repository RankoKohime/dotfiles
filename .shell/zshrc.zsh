# Key Setups
#bindkey -e
  # also do history expansion on space
#bindkey ' ' magic-space
  # setup backspace correctly
#  stty erase `tput kbs`
  # Alternate Backspace
#  stty erase ^?
  # Insert Key
#bindkey '^[[2~' overwrite-mode
  # Delete Key
#bindkey '^[[3~' delete-char
  # Home Key
#bindkey '^[[1~' beginning-of-line
  # End Key
#bindkey '^[[4~' end-of-line
  # Beginning of History
#bindkey '^[[5~' beginning-of-history    
  # End of History
#bindkey '^[[6~' end-of-history          
  # Tab Completion
#bindkey '^i' expand-or-complete-prefix  
#bindkey '^[[1;5D' backward-word
#bindkey '^[[1;5C' forward-word

# History
setopt HIST_IGNORE_SPACE
setopt APPEND_HISTORY
HISTFILE=/home/ranko/.shell/shell-history/history-`whoami`@`cat /etc/hostname`
HISTSIZE=999999999
SAVEHIST=999999999
HIST_STAMPS="yyyy-mm-dd"

# Custom Options
setopt beep extendedglob notify
setopt AUTO_CD
setopt RM_STAR_WAIT

# Aliases
source /home/ranko/.shell/aliasrc

# Custom environment variables
source /home/ranko/.shell/environment
ZDOTDIR=/home/ranko/.shell
