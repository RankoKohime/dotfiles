#!/bin/bash

# Grab all submodules
git submodule update --init --recursive

# Download Vim Plugins
vim +PluginInstall +qall
