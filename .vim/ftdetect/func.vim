" https://stackoverflow.com/questions/11666170/persistent-set-syntax-for-a-given-filetype
" https://waylonwalker.com/vim-augroup/
augroup filetype_shell_function
  au!
  autocmd BufNewFile,BufRead *.func   set syntax=sh
augroup END
