" Plugin settings
  " Airline
    " Smarter tab line
    " Automatically displays all buffers when there's only one tab open.
let g:airline#extensions#tabline#enabled = 1
    " Status bar on top
let g:airline_statusline_ontop = 1
    " Use Devicons
let g:airline_powerline_fonts = 1
    " Enable Hardtime by default
let g:hardtime_default_on = 1
