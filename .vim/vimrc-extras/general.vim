" ================ General Config ====================
    " Change color scheme
set background=dark
colorscheme candycode
    " File-type highlighting and configuration.
    " Run :filetype (without args) to see what you may have
    " to turn on yourself, or just set them all to be sure.
filetype plugin indent on
    "Toggle whether the pasting includes or excludes indents
set pastetoggle=<F2>
    " Disable showmode, as it is superfluous with vim-airline installed
set noshowmode
    " Enable syntax highlighting
syntax on
    " Needed for vim-devicons
set encoding=UTF-8
    " TABS TABS TABS
set expandtab     " Replace tabs with spaces
set softtabstop=2 " Tabs output to spaces
set shiftwidth=2  " Affects indenting
set autoindent    " Automatically indent newlines 
set smartindent   " Do that ^^ smartly
set smarttab      " Because Dumb tabs would be... Dumb!
    " Shows line numbers on the left
set number
    " Highlight the cursor, so we know where it bloody is.
set cursorline
set cursorcolumn
    " Allow backspace in insert mode
set backspace=indent,eol,start
    " Test Backspace (?)
"set backspace=^?
    " Store lots of :cmdline history
set history=1000
    " Show incomplete cmds down the bottom
set showcmd
    " Disable cursor blink
"set gcr=a:blinkon0
    " No sounds
"set visualbell
    " Reload files changed outside vim
set autoread
    " Disable that stupid fucking Visual mouse mode
set mouse-=a
    " Let's try disabling the status line and see if it fiddles with airline
set statusline=0
    " Move viewport with cursor plus offset
set scrolloff=3
    " Make a backup before writing file
set backup
    " Enable spellchecking
set spell
set spelllang=en
    " Split behavior
set splitbelow
set splitright
    " Search behavior
set hlsearch    " Highlight search terms...
set incsearch   " ...dynamically as they are typed.
set ignorecase  " Ignore case during /-style searches, plus VVV
set smartcase   " Case-sensitive searches only if capital letter in search text
