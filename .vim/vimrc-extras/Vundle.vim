" ============ Setup Vundle plugin manager ============
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" List plugins here
    " Noodle Timer
Plugin 'yukiomoto/noodle.vim'

    " Airline Status Bar & Themes
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

    " Git Wrapper
Plugin 'tpope/vim-fugitive'

    " Dev Icons
Plugin 'ryanoasis/vim-devicons'

    " Markdown
Plugin 'godlygeek/tabular'
Plugin 'preservim/vim-markdown'
        " Shortcut keys for vim-markdown:
            " zr: reduces fold level throughout the buffer
            " zR: opens all folds
            " zm: increases fold level throughout the buffer
            " zM: folds everything all the way
            " za: open a fold your cursor is on
            " zA: open a fold your cursor is on recursively
            " zc: close a fold your cursor is on
            " zC: close a fold your cursor is on recursively

    " Python
      " Code formatting
Plugin 'psf/black'

    " Teach yourself the right way
      " Disable arrow keys when overused
Plugin 'takac/vim-hardtime'


" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()

" ============= End Vundle Section ====================
