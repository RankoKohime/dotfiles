    " Let's fix gvim for good
if has("gui_running")
    :gui
    set guifont=Source\ Code\ Pro\ Regular\ 12
endif
