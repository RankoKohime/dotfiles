    " Use Vim settings, rather then Vi settings (much better!).
    " This must be first, because it changes other options as a side effect.
set nocompatible

    " Disable Ex mode keys as well.
    " https://vi.stackexchange.com/questions/457/does-ex-mode-have-any-practical-use
map q: <Nop>
nnoremap Q <nop>

source ~/.vim/vimrc-extras/Vundle.vim

source ~/.vim/vimrc-extras/Vundle-settings.vim

source ~/.vim/vimrc-extras/gvim.vim

source ~/.vim/vimrc-extras/general.vim

" Fancy auto-indent and close bracket creation
inoremap {<cr> {<cr>}<c-o>0<tab>
inoremap [<cr> [<cr>]<c-o>0<tab>
inoremap (<cr> (<cr>)<c-o>0<tab>


" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc,.png,.jpg

" Navigate splits using Ctrl+h/j/k/l
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

