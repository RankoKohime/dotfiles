#!/bin/bash
set +x
#trap read debug

# v 1.0, built for yggdrasil-3

# This script handles a variety of different audio configurations, as requested by the window manager (Awesome)


# Interfaces

#     These functions should automatically adjust the interface variable if the interface changes.
#     This is especially a problem with SPK_FACE is it will change when the monitor is swapped.

# Available Microphone Configs
ASROCK_Z97_E6_MIC_CARD=alsa_card.pci-0000_00_1b.0
ASROCK_Z97_E6_MIC_INPUT=alsa_input.pci-0000_00_1b.0
BEHRINGER_1202_MIXER_CARD=alsa_card.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00
BEHRINGER_1202_MIXER_INPUT=alsa_input.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00
FOCUSRITE_MIC_CARD=alsa_card.usb-Focusrite_Scarlett_Solo_USB-00
FOCUSRITE_MIC_INPUT=alsa_input.usb-Focusrite_Scarlett_Solo_USB-00

# Current Selected Config
MIC_CARD=$BEHRINGER_1202_MIXER_CARD
MIC_INPUT=$BEHRINGER_1202_MIXER_INPUT


GRAPHICS_CARD=alsa_card.pci-0000_01_00.1
GRAPHICS_OUTPUT=
B326HK=alsa_output.pci-0000_01_00.1.hdmi-stereo-extra2
B326HKS=output:hdmi-stereo-extra2
PG279Q=alsa_output.pci-0000_01_00.1.hdmi-stereo
PG279QS=output:hdmi-stereo

MIC_VOL=6554  # This is 10%, also marked as base in Pavucontrol for Asrock Z97
              # Extreme 6 Analog Microphone input
WEB_VOL=9619  # This is 15%, also marked as base in Pavucontrol for ?  C920?

MIC_FACE='$MIC_INPUT'.`pactl list cards|awk -F ":" '/'$MIC_CARD'/{while (!match($0,"Active Profile")) getline;print $4;exit;}'`
SPK_FACE=alsa_output.pci-0000_01_00.1.`pactl list cards|awk -F ":" '/alsa_card.pci-0000_01_00.1/{while (!match($0,"Active Profile")) getline;print $3;exit;}'`

# Functions
MUTE_MIC()
(
  pactl set-source-mute $MIC_FACE toggle;
  IS_MUTED_MIC=`pactl list sources|awk '/'$MIC_FACE'/{while (!match($0,"Mute")) getline;print $2;exit;}'`
  if [ $IS_MUTED_MIC = no ]
    then
      notify-send "Microphone" "Is Unmuted"
    else
      notify-send "Microphone" "Is Muted"
  fi
)

FIX_MIC()
(
  pactl set-source-volume $MIC_FACE $MIC_VOL;
  notify-send "Microphone" "Volume set to $(VOL_MIC)"
)

MON_WHICH()
(
  echo $SPK_FACE|awk -F "." '{print $4}'
)

SWAP_SPK()
(
  if [ $SPK_FACE == "$B326HK" ]
    then
      pactl set-card-profile $GRAPHICS_CARD $PG279QS
      notify-send "Monitor Speakers" "Switched to PG279Q"
  elif [ $SPK_FACE == "$PG279Q" ]
    then
      pactl set-card-profile $GRAPHICS_CARD $B326HKS
      notify-send "Monitor Speakers" "Switched to B326HK"
    else
      notify-send "Monitor Speakers" "Alternate output selected, ABORTING"
  fi
  #pactl # In progress
  notify-send "Monitor Speakers" "Switched output to: $SPK_FACE"
)

LOWR_SPK()
(
  pactl set-sink-volume $SPK_FACE -5%;
  notify-send "Monitor Speakers" "Volume set to $(VOL_SPK)"
)

RISE_SPK()
(
  pactl set-sink-volume $SPK_FACE +5%;
  notify-send "Monitor Speakers" "Volume set to $(VOL_SPK)"
)

MUTE_SPK()
(
  pactl set-sink-mute $SPK_FACE toggle;
  IS_MUTED_SPK=`pactl list sinks|awk '/'$SPK_FACE'/{while (!match($0,"Mute")) getline;print $2;exit;}'`
  if [ $IS_MUTED_SPK = no ]
    then
      notify-send "Monitor Speakers" "Is Unmuted"
    else
      notify-send "Monitor Speakers" "Is Muted"
  fi
)

# Volume tests
VOL_MIC()
(
  pactl list sources|awk '/'$MIC_FACE'/{while (!match($0,"Volume")) getline; print $5; exit; }'
)

VOL_SPK()
(
  pactl list sinks|awk '/'$SPK_FACE'/{while (!match($0,"Volume")) getline; print $5; exit; }'
)

TEST()
(
  # Tests that variables are being set correctly
  echo $MIC_CARD
  echo $MIC_INPUT
  echo $
)
# Process input commands
case $1 in
        micmute)MUTE_MIC;;
        miclkvl)FIX_MIC;;
        spkmute)MUTE_SPK;;
        spkvlup)RISE_SPK;;
        spkvldn)LOWR_SPK;;
        spkswap)SWAP_SPK;;
           test)TEST;;
              *)echo "Audio Control: No valid command specified";
                notify-send "Audio Control: Error" "No valid command specified";;
esac
