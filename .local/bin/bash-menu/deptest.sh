#!/bin/bash

# Tests dependencies for the menu by automatically listing all called apps
# It's gonna be a messy grep-fest, because it seems we can't use an exclude list.

# Get script directory
SCRIPTPATH=$(dirname `readlink -f "$0"`)

# Mash all function files together, and start teh strippin'
cat $SCRIPTPATH/*/*.func \
  | sed -e 's/\"/./g' -e "s/'[^']*'//g" -e 's/"[^"]*"//g' \
  | sed -e "s/^[[:space:]]*#.*$//" -e "s/\([^\\]\)#[^\"']*$/\1/" \
  | sed -e "s/&&/;/g" -e "s/||/;/g" | tr ";|" "\n\n" \
  | awk '{print $1}' | sort -u | uniq \
  | grep -v -f $SCRIPTPATH/exclude.grep
